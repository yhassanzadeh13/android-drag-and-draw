package com.example.yahya.draganddraw;

import android.graphics.PointF;

/**
 * Created by Yahya on 3/4/2018.
 */

public class Box
{
    private PointF mOrigin;
    private PointF mCurrent;

    public Box(PointF origin)
    {
        mOrigin = origin;
        mCurrent = origin;
    }

    public PointF getOrigin()
    {
        return mOrigin;
    }

    public PointF getCurrent()
    {
        return mCurrent;
    }

    public void setCurrent(PointF current)
    {
        mCurrent = current;
    }
}
