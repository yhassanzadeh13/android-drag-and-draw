package com.example.yahya.draganddraw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yahya on 3/1/2018.
 */

public class BoxDrawingView extends View
{
    public static final String TAG = "BoxDrawingView";
    private Paint mBoxPaint;
    private Paint mBackgroundPaint;
    private Box mCurrentBox;
    private List<Box> mBoxen = new ArrayList<>();


    public BoxDrawingView(Context context)
    {
        this(context, null);
    }

    //Used when inflating the view from XML
    public BoxDrawingView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        //Paint the boxes in red
        mBoxPaint = new Paint();
        mBoxPaint.setColor(0x22ff0000);

        //Paint the background in white
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(0xfff8efe0);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        PointF current = new PointF(event.getX(), event.getY());

        String action = new String();
        if(event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN )
        {
          int pointerIndex = event.getActionIndex();
          int pointerId = event.getPointerId(pointerIndex);
        }

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                action = "ACTION_DOWN";
                //reset drawing state
                mCurrentBox = new Box(current);
                mBoxen.add(mCurrentBox);
                break;
            case MotionEvent.ACTION_MOVE:
                action = "ACTION_MOVE";
                if (mCurrentBox != null)
                {
                    mCurrentBox.setCurrent(current);
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                action = "ACTION_UP";
                mCurrentBox = null;
                break;
            case MotionEvent.ACTION_CANCEL:
                action = "ACTION_CANCEL";
                mCurrentBox = null;
                break;
        }
        Log.i(TAG, action + " at x = " + current.x + " , y = " + current.y);
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        // Fill the background
        canvas.drawPaint(mBackgroundPaint);
        for (Box box : mBoxen)
        {
            float left = Math.min(box.getOrigin().x, box.getCurrent().x);
            float right = Math.max(box.getOrigin().x, box.getCurrent().x);
            float top = Math.min(box.getOrigin().y, box.getCurrent().y);
            float bottom = Math.max(box.getOrigin().y, box.getCurrent().y);
            canvas.drawRect(left, top, right, bottom, mBoxPaint);
        }
    }

    @Override
    public Parcelable onSaveInstanceState()
    {
        //begin boilerplate code that allows parent classes to save state
        Parcelable superState = super.onSaveInstanceState();

        SavedState ss = new SavedState(superState);
        //end

        ss.setBoxList(mBoxen);

        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        //begin boilerplate code so parent classes can restore state
        if (!(state instanceof SavedState))
        {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        //end

        this.mBoxen = ss.getBoxList();
    }

    static class SavedState extends BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>()
                {
                    public SavedState createFromParcel(Parcel in)
                    {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size)
                    {
                        return new SavedState[size];
                    }
                };
        private List<Box> boxList;

        SavedState(Parcelable superState)
        {
            super(superState);
        }

        private SavedState(Parcel in)
        {
            super(in);
            in.readList(boxList, Box.class.getClassLoader());
        }

        public void setBoxList(List<Box> boxList)
        {
            this.boxList = boxList;
        }

        @Override
        public void writeToParcel(Parcel out, int flags)
        {
            super.writeToParcel(out, flags);
            out.writeList(boxList);
        }

        public List<Box> getBoxList()
        {
            return boxList;
        }
    }

    private Point rotatePoint(Point rotationOrigin, Point inputPoint, float angle)
    {
        double s = Math.sin(angle);
        double c = Math.cos(angle);

        // translate point back to origin:
        inputPoint.x -= rotationOrigin.x;
        inputPoint.y -= rotationOrigin.y;

        // rotate point
        double xnew = inputPoint.x * c - inputPoint.y * s;
        double ynew = inputPoint.x * s + inputPoint.y * c;

        // translate point back:
        inputPoint.x = (int) xnew + rotationOrigin.x;
        inputPoint.y = (int) ynew + rotationOrigin.y;
        return inputPoint;
    }

}
